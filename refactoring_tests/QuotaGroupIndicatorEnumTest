package com.vsct.wdi.rbm.train;

import static com.vsct.wdi.rbm.train.QuotaGroupIndicatorEnum.IMPOSSIBLE;
import static com.vsct.wdi.rbm.train.QuotaGroupIndicatorEnum.NO_RESTRICTION;
import static com.vsct.wdi.rbm.train.QuotaGroupIndicatorEnum.RESTRICTED;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

public class QuotaGroupIndicatorEnumTest {
	@Test
	public void testGetInstanceFromRrQgIndicator() throws Exception {
		assertThat(QuotaGroupIndicatorEnum.getInstanceFromRrQgIndicator(null)).isEqualTo(NO_RESTRICTION);

		assertThat(QuotaGroupIndicatorEnum.getInstanceFromRrQgIndicator("1")).isEqualTo(RESTRICTED);

		assertThat(QuotaGroupIndicatorEnum.getInstanceFromRrQgIndicator("2")).isEqualTo(IMPOSSIBLE);
	}

	@Test
	public void testGetInstance() throws Exception {
		assertThat(QuotaGroupIndicatorEnum.getInstance(null)).isNull();
		assertThat(QuotaGroupIndicatorEnum.getInstance("fake")).isNull();

		assertThat(QuotaGroupIndicatorEnum.getInstance("NO_RESTRICTION")).isEqualTo(NO_RESTRICTION);
		assertThat(QuotaGroupIndicatorEnum.getInstance("RESTRICTED")).isEqualTo(RESTRICTED);
		assertThat(QuotaGroupIndicatorEnum.getInstance("IMPOSSIBLE")).isEqualTo(IMPOSSIBLE);
	}

	@Test
	public void testIsGroupSellingEnabled() throws Exception {
		assertThat(NO_RESTRICTION.isGroupSellingEnabled()).isTrue();

		assertThat(RESTRICTED.isGroupSellingEnabled()).isFalse();
		assertThat(IMPOSSIBLE.isGroupSellingEnabled()).isFalse();
	}
}
