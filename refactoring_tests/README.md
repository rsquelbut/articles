# Refactoring des tests #

# Premier constat #

Ainsi sur les dernières missions (dans des contextes favorables au Clean Code) où je suis passé, 
j'ai trouvé du code avec une qualité oscillant entre bon et mauvais, mais rien de choquant. 
Avec quelques efforts, l'intention de chaque portion de code était trouvable.

Par contre, j'ai été parfois choqué par le code produit dans les TU. 
Même parfois écrit par des développeurs reconnus comme experts techniques au sein de la mission, le code était horrible.

Qu'est ce qui peut expliquer celà ?
# Analyse au doigt mouillé
J'ai l'impression que suivre les _"best practises"_ de développement est devenu de plus en plus courant. De même que faire des TU. 
Le pourquoi est bien compris et les efforts sont faits dans ce sens.

Cependant on dirait que le _pourquoi_ du TU n'est pas toujours bien compris.
En effet, au sens premier, quand on entend _Test Unitaire_, c'est le mot _test_ qui reste en tête. 
Ainsi, de prime abord on risque de tester le comportement de la méthode au lieu de son intention.
TODO trouver un vrai exemple
Par exemple, on veut tester la méthode `public boolean isMajor() {...}`, 
le premier réflexe est de créer une méthode `public void isMajorShouldReturnTrueWhen`

Lorsque l'on créait une classe de Test JUnit avec Eclipse, il nous était proposé d'utiliser une méthode de test par méthode à tester. 
Ceci à d'ailleurs contribué à mes débuts difficiles avec les TU quand j'étais seul à en faire dans mon équipe. 

Ceci m'amène au point suivant : alors que j'ai toujours porté beaucoup d'attention à l'élégance de mon code,
j'ai mis du temps à m'intéresser à celle de mes tests. 
Deux raisons à celà. Une fois que j'avais "réussi" à tester mon code, j'étais satisfait.
D'autre part, on fait des TU

Le premier jet d'un morceau de code est rarement élégant ou compréhensible.



Il n'y a pas d'outil de non régression des TU
Les API de mock, trés puissantes mais très techniques, fournissent du code parfois difficile à appréhender
( *TODO* voir exemple TU)
L'initialisation des données représente régulièrement 90% du code de test
( *TODO* fournir exemple)
Le problème du test des méthodes privées
( *TODO* lien article _@cafetux_)

# Titre à trouver #
Comme beaucoup de métiers, escalade, peinture,  nous avons besoin d'un filet de sécurité pour exercer notre métier dans de bonnes conditions. 
Mais contrairement à ces métiers, la mise en place du filet de sécurité est la même activité aue notre métier.
Et oui, faire des TU, c'est faire du développement. Il n'y a donc pas de raison à ne pas aimer faire des TU.
Il n'y a pas de raison, ni d'un point de vue fonctionnel, ni d'un point de vue personnel à négliger la mise en place de TU.

# Faire des TU, cest dur #

# les TU sont souvent négligés #


- scotch ou bâche pour faire des peintures propres

exemple de nom de méthode pourri
```public void updateWithSQueryResult_should_be_ok() {...}```
```public void updateWithSQueryResult_should_be_NOK() {...}```
- 
